package cockroach

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

func NewConn(addr, db string) (*sql.DB, error) {
	conn := fmt.Sprintf("postgresql://%s/%s?sslmode=disable", addr, db)
	database, error := sql.Open("postgres", conn)
	if error != nil {
		log.Fatal(error.Error())
	} else {
		loadModel(database)
	}
	return database, error
}

func loadModel(db *sql.DB) {
	if _, err := db.Exec(
		"CREATE TABLE IF NOT EXISTS queryChallenge (id UUID NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY, domain STRING)"); err != nil {
		log.Fatal(err)
	}
}
