package cockroach

import (
	"database/sql"
	"fmt"
	"log"
	"os"
)

type QueryChallengeRepository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) QueryChallengeRepository {
	return QueryChallengeRepository{db: db}
}

func (repository QueryChallengeRepository) CreateQueryChallenge(url string) error {
	query := fmt.Sprintf("INSERT INTO queryChallenge (domain) VALUES ('%s')", url)
	if _, err := repository.db.Exec(query); err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func (repository QueryChallengeRepository) SearchQueryChallenge(url string) bool {
	query := fmt.Sprintf("SELECT domain FROM queryChallenge WHERE domain='%s'", url)
	rows, err := repository.db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	return rows.Next()
}

func (repository QueryChallengeRepository) GetAllOldRequest() []string {
	oldRequest := []string{}
	query := "SELECT domain FROM queryChallenge"
	rows, err := repository.db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var domain string
		if err := rows.Scan(&domain); err != nil {
			log.Fatal(err)
		}
		oldRequest = append(oldRequest, domain)
	}
	return oldRequest
}

func NewCockroachRepository() QueryChallengeRepository {
	cockroachAddr := os.Getenv("COCKROACH_ADDR")
	cockroachDBName := os.Getenv("COCKROACH_DB")
	cockroachConn, err := NewConn(cockroachAddr, cockroachDBName)
	if err != nil {
		log.Fatal(err)
	}
	return NewRepository(cockroachConn)
}
