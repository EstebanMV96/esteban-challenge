package api

import (
	"esteban-challenge/controller"

	"github.com/gin-gonic/gin"
)

func EndpointsV1(server *gin.Engine) {
	server.GET("api/v1/info", controller.GetServerInfo)
	server.GET("api/v1/items", controller.GiveConsultedServers)
}
