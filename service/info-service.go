package service

import (
	"esteban-challenge/database/cockroach"
	"esteban-challenge/dto"
	"strings"
)

func GetOldRequest() dto.AllRequest {
	var oldRequest dto.AllRequest
	repository := cockroach.NewCockroachRepository()
	oldRequest.Items = repository.GetAllOldRequest()
	removeHTTPS(oldRequest.Items)
	return oldRequest
}

func removeHTTPS(oldRequest []string) {
	for index, request := range oldRequest {
		oldRequest[index] = strings.TrimPrefix(request, "https://")
	}
}
