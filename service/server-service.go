package service

import (
	"encoding/json"
	"esteban-challenge/database/cockroach"
	"esteban-challenge/dto"
	"esteban-challenge/middleware"
	"esteban-challenge/middleware/redis"
	"log"
	"sort"
	"strings"
)

func GetServerInfo(url string) dto.Company {
	saveDomain(url)
	serverInfo := dto.Company{}
	serverInfo = dto.MapSslabReponsetoCompany(middleware.AnalyzeUrl(url))
	setServerInformation(serverInfo)
	serverInfo.Logo = getLogo(url)
	serverInfo.Title = middleware.GetTitle(url)
	serverInfo.SslGrade = getLowerSslGrade(serverInfo)
	serverInfo.IsDown = !middleware.SiteIsAlive(url)
	serverInfo = setOldData(url, serverInfo)
	return serverInfo
}

func setOldData(url string, serverInfo dto.Company) dto.Company {
	oldData := redis.GetValue(url)
	if len(oldData) == 0 {
		serverInfo.PreviousSslGrade = serverInfo.SslGrade
		serverInfo.ServersChanged = false
		dataJSON, err := json.Marshal(&serverInfo)
		if err != nil {
			log.Fatal(err)
		}
		redis.SaveValue(url, string(dataJSON))
	} else {
		bytes := []byte(oldData)
		var oldInfo dto.Company
		err := json.Unmarshal(bytes, &oldInfo)
		if err != nil {
			log.Fatal(err)
		}
		serverInfo.PreviousSslGrade = oldInfo.PreviousSslGrade
		serverInfo.ServersChanged = isInfoChanged(serverInfo, oldInfo)
	}
	return serverInfo
}

func isInfoChanged(info dto.Company, oldInfo dto.Company) bool {
	infoChanged := false
	if len(info.Servers) != len(oldInfo.Servers) {
		infoChanged = true
	} else {

		if info.Title != oldInfo.Title || info.Logo != oldInfo.Logo {
			infoChanged = true
		} else {
			for i := 0; i < len(info.Servers); i++ {
				serverA := info.Servers[i]
				serverB := oldInfo.Servers[i]
				if serverA.Address != serverB.Address || serverA.SslGrade != serverB.SslGrade || serverA.Country != serverB.Country || serverA.Owner != serverB.Owner {
					infoChanged = true
				}
			}
		}

	}
	return infoChanged
}

func setServerInformation(serverInfo dto.Company) {
	for index, server := range serverInfo.Servers {
		serverInfo.Servers[index] = setOwnerAndCountryCode(server)
	}
}

func getLogo(url string) string {
	logo := "No es posible consultar el logo"
	logos := middleware.GetLogos(url)
	if len(logos) > 0 {
		// You can modify json to show all logos
		logo = logos[0]
	}
	return logo
}

func saveDomain(url string) {
	repository := cockroach.NewCockroachRepository()
	if !repository.SearchQueryChallenge(url) {
		repository.CreateQueryChallenge(url)
	}
}

func setOwnerAndCountryCode(serverInfo dto.Server) dto.Server {
	command := "whois"
	result := middleware.ExecuteCommand(command, serverInfo.Address)
	organization := strings.Split(result, "OrgName:")
	if len(organization) > 1 {
		organization = strings.Split(organization[1], "OrgId:")
		country := strings.Split(result, "Country:")
		country = strings.Split(country[1], "RegDate:")
		serverInfo.Country = strings.TrimSuffix(strings.Trim(country[0], " "), "\n")
		serverInfo.Owner = strings.TrimSuffix(strings.Trim(organization[0], " "), "\n")
	}

	return serverInfo
}

func getLowerSslGrade(serverInfo dto.Company) string {
	lowerSslGrade := ""
	grades := []string{}
	for _, server := range serverInfo.Servers {
		grades = append(grades, server.SslGrade)
	}
	if len(grades) > 0 {
		sort.Strings(grades)
		lowerSslGrade = grades[len(grades)-1]
	}
	return lowerSslGrade
}
