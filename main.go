package main

import (
	"esteban-challenge/api"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	server := gin.Default()
	server.Use(cors.Default())
	api.EndpointsV1(server)
	server.Run(":5000")
}
