package controller

import (
	"esteban-challenge/service"
	"fmt"

	"github.com/gin-gonic/gin"
)

func GetServerInfo(context *gin.Context) {
	url := context.Query("server")
	url = addHTTPS(url)
	context.JSON(200, service.GetServerInfo(url))
}

func GiveConsultedServers(context *gin.Context) {
	context.JSON(200, service.GetOldRequest())
}

func addHTTPS(url string) string {
	return fmt.Sprintf("https://%s", url)
}
