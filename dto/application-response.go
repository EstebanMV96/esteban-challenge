package dto

type Company struct {
	Servers          []Server `json:"servers"`
	ServersChanged   bool     `json:"servers_changed"`
	SslGrade         string   `json:"ssl_grade"`
	PreviousSslGrade string   `json:"previous_ssl_grade"`
	Logo             string   `json:"logo"`
	Title            string   `json:"title"`
	IsDown           bool     `json:"is_down"`
}

type Server struct {
	Address  string `json:"address"`
	SslGrade string `json:"ssl_grade"`
	Country  string `json:"country"`
	Owner    string `json:"owner"`
}

type AllRequest struct {
	Items []string
}

func MapSslabReponsetoCompany(response SslLabResponse) Company {
	company := Company{}
	for _, labServer := range response.Servers {
		server := Server{}
		server.Address = labServer.Address
		server.SslGrade = labServer.SslGrade
		company.Servers = append(company.Servers, server)
	}
	return company
}
