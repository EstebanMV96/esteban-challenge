package dto

type SslLabResponse struct {
	Servers []SslLabResponseServer `json:"endpoints"`
}

type SslLabResponseServer struct {
	Address  string `json:"ipAddress"`
	SslGrade string `json:"grade"`
}
