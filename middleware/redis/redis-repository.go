package redis

import (
	"log"
	"time"

	"github.com/go-redis/redis"
)

const expirationTime = 1 * time.Hour

const endpointRedis = "challenge-dev-redis.o3nopx.ng.0001.use1.cache.amazonaws.com:6379"

func SaveValue(key string, value string) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     endpointRedis,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	err := rdb.Set(key, value, expirationTime).Err()
	if err != nil {
		log.Fatal(err)
	}
}

func GetValue(key string) string {

	value := ""
	rdb := redis.NewClient(&redis.Options{
		Addr:     endpointRedis,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	value, _ = rdb.Get(key).Result()

	return value
}
