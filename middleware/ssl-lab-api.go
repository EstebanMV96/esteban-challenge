package middleware

import (
	"encoding/json"
	"esteban-challenge/dto"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

const url string = "https://api.ssllabs.com/api/v3"

func AnalyzeUrl(domain string) dto.SslLabResponse {
	var responseData dto.SslLabResponse
	petition := fmt.Sprintf(url+"/analyze?host=%s", domain)
	response, error := http.Get(petition)
	if error != nil {
		fmt.Print(error.Error())

	} else {
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err.Error())
		}
		err1 := json.Unmarshal(body, &responseData)
		if err1 != nil {
			log.Fatal(err1.Error())
		}
	}

	return responseData
}
