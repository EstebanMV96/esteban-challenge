package middleware

import (
	"strings"

	"github.com/gocolly/colly"
)

func GetLogos(url string) []string {
	logos := []string{}
	c := colly.NewCollector()

	c.OnHTML("head link", func(e *colly.HTMLElement) {

		resourceType := e.Attr("rel")
		link := e.Attr("href")
		if strings.Contains(resourceType, "icon") {
			logos = append(logos, link)
		}

	})

	c.Visit(url)
	return logos
}

func GetTitle(url string) string {
	title := ""
	c := colly.NewCollector()

	c.OnHTML("head title", func(e *colly.HTMLElement) {
		title = e.Text
	})

	c.Visit(url)
	return title
}

func SiteIsAlive(url string) bool {

	c := colly.NewCollector()
	error := c.Visit(url)
	return error == nil
}
