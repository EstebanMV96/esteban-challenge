package middleware

import (
	"log"
	"os/exec"
)

func ExecuteCommand(command string, param string) string {
	cmd := exec.Command(command, param)
	result, err := cmd.Output()
	if err != nil {
		log.Fatal(err.Error())
	}
	return string(result)
}
